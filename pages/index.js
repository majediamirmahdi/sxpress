import FiltersBar from '../components/home/filters-bar'
import ProductList from '../components/home/product-list'
import PaginationSection from '../components/home/pagination-section'

import mockData from "./mock.json"

const spaced = { marginTop: 16, marginBottom: 16 }

export default function Home() {
  return (<>
    <div style={spaced}>
      <FiltersBar
        categoriesData={mockData.data.categories}
        sortData={mockData.data.meta.sort}
        filterData={mockData.data.meta.filter}
      />
    </div>
    <div style={spaced}>
      <ProductList products={mockData.data.product_variations} loaded={true} />
    </div>
    <div style={spaced}>
      <PaginationSection paginationData={mockData.data.meta.pagination} loaded={true} />
    </div>
  </>)
}