import { useEffect } from "react";

import Layout from "../components/layout"
import Theme from "../components/theme"

import api from "../api"

import '../styles/globals.css'

export default function MyApp({ Component, pageProps }) {

  useEffect(() => {
    let mounted = true;
    window.$api = api
    return () => mounted = false;
  }, [])

  return (<Theme>
    <Layout>
      <Component {...pageProps} />
    </Layout>
  </Theme>)
}