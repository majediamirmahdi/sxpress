import axios from "axios"

import { toast } from 'react-toastify';

import services from "./services"

const BASE_URL = "https://api.snapp.express"

const TOAST_CONFIG = {
  position: "bottom-left",
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
}

axios.defaults.baseURL = BASE_URL;

axios.interceptors.response.use(function (config) {
});

const api = {};

for (let service in services) {
  let newService = {}
  for (let action in services[service]) {
    newService[action] = async function() {
      try {
        let { data } = await services[service][action].apply(axios, arguments);
        toast.success('عملیات با موفقیت انجام شد', TOAST_CONFIG);
        return data
      } catch (error) {
        toast.error('خطایی رخ داده. لطفا مجددا تلاش کنید', TOAST_CONFIG);
        throw error
      }
    }
  }
  api[service] = newService
}

api.axios = axios

export default api;