import productsService from "./products"

const services = {
    products: { ...productsService },
}

export default services;