import {
    COURSES,
    COURSE,
    REMOVE_COURSE,
    ADD_COURSE,
    FINALIZE_COURSES,
    RESET_COURSES,
} from "./endpoints"

export default {
    async getCourses() {
        return this.get(COURSES)
    },
    async resetCourses() {
        return this.post(RESET_COURSES)
    },
    async finalizeCourses() {
        return this.post(FINALIZE_COURSES)
    },
    async getCourse(id) {
        return this.get(COURSE.replace('__COURSE_ID__', id))
    },
    async removeCourse(id) {
        return this.delete(REMOVE_COURSE.replace('__COURSE_ID__', id))
    },
    async addCourse(id) {
        return this.post(ADD_COURSE.replace('__COURSE_ID__', id))
    },
}