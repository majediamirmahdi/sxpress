import VazirBlackTTF from "./Black/Vazir-Black.ttf";
import VazirBoldTTF from "./Bold/Vazir-Bold.ttf";
import VazirLightTTF from "./Light/Vazir-Light.ttf";
import VazirMediumTTF from "./Medium/Vazir-Medium.ttf";
import VazirRegularTTF from "./Regular/Vazir.ttf";
import VazirThinTTF from "./Thin/Vazir-Thin.ttf";

const vazirBase = {
  fontStyle: 'normal',
  fontFamily: "Vazir",
  fontDisplay: "swap",
  unicodeRange: "U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF",
};

export const vazirBlack = {
  ...vazirBase,
  fontWeight: 900,
  src: `url(${VazirBlackTTF}) format('ttf')`,
};

export const vazirBold = {
  ...vazirBase,
  fontWeight: 'bold',
  src: `url(${VazirBoldTTF}) format('ttf')`,
};

export const vazirMedium = {
  ...vazirBase,
  fontWeight: 500,
  src: `url(${VazirMediumTTF}) format('ttf')`,
};

export const vazirRegular = {
  ...vazirBase,
  fontWeight: 'normal',
  src: `url(${VazirRegularTTF}) format('ttf')`,
};

export const vazirLight = {
  ...vazirBase,
  fontWeight: 300,
  src: `url(${VazirLightTTF}) format('ttf')`,
};

export const vazirThin = {
  ...vazirBase,
  fontWeight: 100,
  src: `url(${VazirThinTTF}) format('ttf')`,
};
