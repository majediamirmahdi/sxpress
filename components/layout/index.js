import Container from '@mui/material/Container';

import { ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

export default function Layout ({ children }) {

  return (<>
    <main dir="rtl">
      <Container maxWidth="xl">
        {children}
      </Container>
    </main>
    <ToastContainer
      position="bottom-left"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop
      closeOnClick
      rtl
      pauseOnFocusLoss
      draggable
      pauseOnHover
      toastClassName={'font-vazir'}
    />
  </>);
}