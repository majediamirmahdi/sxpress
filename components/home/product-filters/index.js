import { Accordion, AccordionSummary, Typography, AccordionDetails } from "@mui/material"

import Icon from '@mdi/react'
import { mdiChevronDown } from '@mdi/js'

export default function ProductFilters({ filterData }) {

  return (<>
    {filterData.results.map((filterResult, idx) => (<Accordion key={idx}>
      <AccordionSummary
        expandIcon={<Icon path={mdiChevronDown}
          size={1}
          horizontal
          vertical
        />}
        aria-controls="panel2a-content"
        id="panel2a-header"
      >
        <Typography>{filterResult.translation}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          malesuada lacus ex, sit amet blandit leo lobortis eget.
        </Typography>
      </AccordionDetails>
    </Accordion>))}
  </>)
}