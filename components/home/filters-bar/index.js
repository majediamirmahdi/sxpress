import { useState, forwardRef } from 'react'

import { Slide, Dialog, Grid, FormControl, InputLabel, Select, MenuItem, Button } from '@mui/material';

import Icon from '@mdi/react'
import { mdiSortVariant, mdiFilter, mdiClose } from '@mdi/js'

import ProductFilters from '../../../components/home/product-filters'

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


export default function FiltersBar({ categoriesData, sortData, filterData }) {

  let [dialog, setDialog] = useState(false);

  let sortValueInit = sortData.results.find(i => (i.enabled == true))?.name || ''
  let [sort, setSort] = useState(sortValueInit);

  const handleChange = (name) => (event) => {
    let val = event.target.value
    switch (name) {
      case 'sort':
        setSort(val);
        break;
    }
  };

  return (<>
    <Dialog
      dir="rtl"
      fullScreen
      open={dialog}
      onClose={() => setDialog(false)}
      TransitionComponent={Transition}
    >
      <div style={{ height: '100%', padding: 16, display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
        <div>
          <ProductFilters filterData={filterData}></ProductFilters>
        </div>
        <Button onClick={() => setDialog(false)} style={{ width: '100%' }} variant="contained" endIcon={<Icon path={mdiClose}
          style={{ marginRight: -12, marginLeft: 12 }}
          size={1}
        />}>
          بستن
        </Button>
      </div>
    </Dialog>
    <Grid justifyContent="flex-start" alignItems="center" container spacing={2} direction="row">
      <Grid style={{ height: '100%' }} item xs={12} md={3}>
        <Button onClick={() => setDialog(true)} style={{ width: '100%' }} variant="contained" startIcon={<Icon path={mdiFilter}
          style={{ marginRight: -12, marginLeft: 12 }}
          size={1}
        />}>
          فیلتر
        </Button>
      </Grid>
      <Grid style={{ height: '100%' }} item xs={12} md={3}>
        <FormControl fullWidth variant="standard">
          <InputLabel dir="rtl" id="sort-select-label">
            مرتب‌سازی
          </InputLabel>
          <Select
            labelId="sort-select-label"
            id="sort-select"
            value={sort}
            onChange={handleChange('sort')}
          >
            {sortData.results.map((item, idx) => <MenuItem key={idx} value={item.name}>{item.translation}</MenuItem>)}
          </Select>
        </FormControl>
      </Grid>
    </Grid>
  </>)
}