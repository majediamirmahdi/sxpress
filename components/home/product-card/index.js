import { useState } from "react"

import { Rating, Card, CardMedia, CardContent, Typography, Button, CardActions, Stack, IconButton } from '@mui/material';

import Icon from '@mdi/react'
import { mdiPlus, mdiMinus } from '@mdi/js'

import numeral from 'numeral'

import ScrollText from '../../../components/scroll-text'

export default function ProductCard({ item }) {
  let [count, setCount] = useState(0)

  return (<Card style={{ height: '100%' }}>
    <CardMedia
      component="img"
      image={item.featured}
      alt={item.productVariationTitle}
    />
    <CardContent>
      <Typography gutterBottom variant="h6" component="div">
        <ScrollText>{item.productVariationTitle}</ScrollText>
      </Typography>
      <Typography variant="body2" color="text.secondary">
        <span style={{ fontWeight: 'bold' }}>{numeral(item.price).format('0,0')}</span> تومان
      </Typography>
    </CardContent>
    <CardActions>
      {
        (count == 0)
          ? (<Button onClick={() => setCount(1)} disabled={(item.capacity == 0)} style={{ width: '100%' }} variant="contained" startIcon={<Icon path={mdiPlus}
            style={{ marginRight: -12, marginLeft: 12 }}
            size={1}
            horizontal
            vertical
          />}>
            افزودن
          </Button>)
          : (<div style={{ width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <IconButton onClick={() => setCount(count + 1)} aria-label="add" disabled={count >= item.capacity}>
              <Icon path={mdiPlus}
                size={1}
                horizontal
                vertical
              />
            </IconButton>
            <div style={{ padding: 4 }}>{count}</div>
            <IconButton onClick={() => setCount(count - 1)} aria-label="remove">
              <Icon path={mdiMinus}
                size={1}
                horizontal
                vertical
              />
            </IconButton>
          </div>)
      }
    </CardActions>
    {/* <Rating name="read-only" value={item.rating / 2} readOnly /> */}
  </Card>)
}