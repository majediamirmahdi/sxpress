import { Grid } from '@mui/material';

import ProductCard from '../../../components/home/product-card'

export default function ProductList({ products, loaded }) {
  return <Grid container spacing={2} direction="row">
    {products.map((item, idx) => (<Grid style={{ height: '100%' }} key={idx} item xs={12} sm={6} md={6} lg={3} xl={3}>
      <ProductCard item={item}></ProductCard>
    </Grid>)
    )}
  </Grid>
}