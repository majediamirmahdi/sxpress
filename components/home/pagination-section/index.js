import { useState } from 'react'

import { Pagination, Grid, FormControl, InputLabel, Select, MenuItem } from '@mui/material';

export default function PaginationSection({ paginationData }) {
  const [page, setPage] = useState(paginationData.page);
  const [count, setCount] = useState(paginationData.size);

  const handleChange = (name) => (event) => {
    let val = event.target.value
    switch (name) {
      case 'page':
        setPage(val);
        break;
      case 'count':
        setCount(val);
        break;
    }
  };

  return (<Grid justifyContent="space-around" alignItems="center" container spacing={2} direction="row">
    <Grid style={{ height: '100%' }} item xs={12} md={4}>
      <FormControl fullWidth variant="standard">
        <InputLabel dir="rtl" id="count-select-label">
          تعداد محصولات
        </InputLabel>
        <Select
          labelId="count-select-label"
          id="count-select"
          value={count}
          onChange={handleChange('count')}
        >
          <MenuItem value={20}>20</MenuItem>
          <MenuItem value={50}>50</MenuItem>
          <MenuItem value={100}>100</MenuItem>
        </Select>
      </FormControl>
    </Grid>
    <Grid style={{ height: '100%' }} item>
      <Pagination size="large" page={page} count={paginationData.total} variant="outlined" color="primary" onChange={handleChange('page')} />
    </Grid>
  </Grid>
  )
}