import styles from "./style.module.css"

export default function ScrollText({ children }) {
  return (<div className={styles.scrollContainer}>
    <div className={styles.scrollText}>{children}</div>
  </div>)
}