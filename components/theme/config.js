import { createTheme } from '@mui/material';

import {
  vazirBlack,
  vazirBold,
  vazirMedium,
  vazirRegular,
  vazirLight,
  vazirThin,
} from "../../styles/fonts/vazir/index"

export default createTheme({
  direction: 'rtl',
  typography: {
    fontFamily: 'Vazir, sans-serif',
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [ vazirBlack, vazirBold, vazirMedium, vazirRegular, vazirLight, vazirThin ],
      },
    },
  },
  palette: {
    type: 'light',
    primary   : { main: '#1fa5ff' },
    secondary : { main: '#1a4c89' },
    success   : { main: '#06d6a0' },
    info      : { main: '#28a745' },
    warning   : { main: '#edbb41' },
    danger    : { main: '#ff595e' },
    light     : { main: '#fbfbfb' },
    dark      : { main: '#292929' },
  },
});
