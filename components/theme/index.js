import { ThemeProvider, CssBaseline } from '@mui/material';

import config from './config';

const Theme = ({ children }) => {
  return <ThemeProvider theme={config}>
    <CssBaseline />
    {children}
  </ThemeProvider>
}

export default Theme